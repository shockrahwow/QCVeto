# New Lib Update
----------------

Rolled out update to stop using outdated graphics library for something more
maintainable. This should make updating the repo so that it lines up with
Quake Champion's updates much faster. 


# Old vs new Features
Feature | Old | New
|---|:---:|:---:|
Multiple Gametypes | | x
Faster Updates| | x
Terminal Based Input | x |  
User friendly packaging | | x
Dynamic Ui | | x
Proper Scaling | | x
