from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.config import Config # for defualt window size --> 1280x720
from kivy.uix.floatlayout import FloatLayout

# NOTE: handling the doubles screen for now since duel is getting an update in like two days
Builder.load_string("""
<MainScreen>:
	Image:
		source: './img/background.jpg'
		keep_ratio: False
		allow_stretch: True
		width: self.parent.width
		height: self.parent.width
	BoxLayout:
		Button:
			text: 'DUEL'
			background_color: (0,0,0,0.6)
			on_press: 
				root.manager.current = 'Duel'
				root.manager.transition.direction = 'right'
		Button:
			text: 'Doubles'
			background_color: (0,0,0,0.6)
			on_press: 
				root.manager.current = 'Doubles'
				root.manager.transition.direction = 'up'
<DuelScreen>:
	# BACKGROUND RED/BLUE TEAM IMAGES
	Image:
		source: './img/background.jpg'
		keep_ratio: False
		allow_stretch: True
		width: self.parent.width
		height: self.parent.height
	# TEAM LOGOS
	Image:
		source: './img/red_team.png'
		size_hint: 0.5, 0.5
		pos_hint: {'x':0.5, 'y': 0.4}
	Image: 
		source: './img/blue_team.png'
		size_hint: 0.5, 0.5
		pos_hint: {'x':0.0, 'y':0.4}
	# CONTAINER FOR ALL INTERACTABLE WIDGETS
	FloatLayout:
		# RED/BLUE TEAM TEXT INPUT
		TextInput:
			size_hint: 0.23, 0.07
			pos_hint: {'x':0.135, 'y':0.4}
			font_size: 18
			multiline: False
			background_normal: './img/textBorder.png'
			background_active: './img/textBorder.png'
		TextInput:
			size_hint: 0.23, 0.07
			pos_hint: {'x':0.635, 'y':0.4}
			font_size: 18
			multiline: False
			background_normal: './img/textBorder.png'
			background_active: './img/textBorder.png'
		
		BoxLayout:
			orientation: 'horizontal'
			size_hint: 1.0, 0.2
			padding: 5	
			ToggleButton:
				text: 'Blood Covenant' 
				background_normal: './img/bloodc.jpg'
				background_down: './img/bloodc_ban.jpg'
			ToggleButton:
				text: 'Ruins of Sarnath'
				background_normal: './img/ruins.jpg'
				background_down: './img/ruins_ban.jpg'
			ToggleButton:
				text: 'Corrupted Keep'
				background_normal: './img/keep.png'
				background_down: './img/keep_ban.png'
			ToggleButton:
				text: 'Bloodrun'
				background_normal: './img/bloodrun.png'
				background_down: './img/bloodrun_ban.png'
			ToggleButton:
				text: 'Vale of Pnath'
				background_normal: './img/pnath.jpg'
				background_down: './img/pnath_ban.jpg'
		Button:
			size_hint: 0.05, 0.05
			pos_hint: {'x':0, 'y':.95}
			text: 'back'
			background_color: (0,0,0,0.6)
			on_press: 
				root.manager.current = 'Main'
				root.manager.transition.direction = 'left'
<DoublesScreen>:
	# BACKGROUND IMAGE
	Image:
		source: './img/background.jpg'
		keep_ratio: False
		allow_stretch: True
		width: self.parent.width
		height: self.parent.height
	# BLUE AND RED TEAM BACKGROUND
	Image:
		source: './img/blue_team.png'
		size_hint: 0.5, 0.5
		pos_hint: {'x':0.0, 'y':0.4}
	Image:
		source: './img/red_team.png'
		size_hint: 0.5, 0.5
		pos_hint: {'x':0.5, 'y':0.4}	
		
	# CONTAINER FOR INTERFACE WIDGETS
	FloatLayout:
		# TEXT INPUT BLUE/RED TEAMS
		TextInput:
			size_hint: 0.23, 0.07
			pos_hint: {'x':0.135, 'y':0.4}
			font_size: 18
			background_normal: './img/textBorder.png'
			background_active: './img/textBorder.png'
		TextInput: 
			size_hint: 0.23, 0.07
			pos_hint: {'x':0.635, 'y':0.4}
			font_size:18
			background_normal: './img/textBorder.png'
			background_active: './img/textBorder.png'
						
		# DOUBLES MAPS
		BoxLayout:
			orientation: 'horizontal'
			size_hint: 1.0 , 0.2
			padding: 5
			ToggleButton:
				text: 'Blood Covenant'
				background_normal: './img/bloodc.jpg'
				background_down: './img/bloodc_ban.jpg'
			ToggleButton:
				text: 'Ruins of Sarnath'
				background_normal: './img/ruins.jpg'
				background_down: './img/ruins_ban.jpg'
			ToggleButton:
				text: 'Burial Chamber'
				background_normal: './img/burial.jpg'
				background_down: './img/burial_ban.jpg'
			ToggleButton:
				text: 'Corrupted Keep'
				background_normal: './img/keep.png'
				background_down: './img/keep_ban.png'
			ToggleButton:
				text: 'Bloodrun'
				background_normal: './img/bloodrun.png'
				background_down: './img/bloodrun_ban.png'
			ToggleButton:
				text: 'Vale of Pnath'
				background_normal: './img/pnath.jpg'
				background_down: './img/pnath_ban.jpg'	
		Button: 
			text: 'back'
			size_hint: 0.05, 0.05
			pos_hint: {'x':0, 'y':0.95}	
			background_color: (0,0,0,0.6)
			on_press: 
				root.manager.current =  'Main'
				root.manager.transition.direction = 'down'

<SacScreen>:
	FloatLayout:
		BoxLayout:
			size_hint: 1.0, 0.1
			sizes_hint: 1.0, 0.1
			padding: 5
			Button:
				text: 'Blood Covenant'
			Button:
				text: 'Ruins of Sarnath'
			Button:
				text: 'Burial Chamber'
			Button:
				text: 'Lockbox'
			Button:
				text: 'Temple of Azathoth'
			Button:
				text: 'Tempest Shrine'	
		Button:
			text: 'back'
			size_hint: 0.05, 0.05
			pos_hint: {'x':0.0, 'y':0.95}
			on_press: 
				root.manager.current = 'Main'
				root.manager.transition.direction = 'right'
""")
Config.set('graphics', 'width', 1280)
Config.set('input', 'mouse', 'mouse,disable_multitouch')
# screen POD's
class MainScreen(Screen):
    pass

class DuelScreen(Screen):
    pass 

class DoublesScreen(Screen):
	pass
class SacScreen(Screen):
	pass
# configure transitions here as well as some other stuff	
# main screen object
sm = ScreenManager()

sm.add_widget(MainScreen(name='Main'))
sm.add_widget(DuelScreen(name='Duel'))
sm.add_widget(DoublesScreen(name='Doubles'))
sm.add_widget(SacScreen(name='Sac'))

class Sample(App):
    def build(self):
        return sm
        
if __name__ == '__main__':
    Sample().run()
